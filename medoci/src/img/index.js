import logoImg from './Medoci logo.png';
import mobileImg from './mobile1.png';
import deliveryImg from './delivery.png';
import client1Img from './client-1.png';
import client2Img from './client-2.png';
import client3Img from './client-3.png';
import client4Img from './client-4.png';
import client5Img from './client-5.png';
import client6Img from './client-6.png';
import EllipseblurImg from './Ellipse-blue.png';
import LogoSigcurePharmacyImg from './Logo-Sigcure-Pharmacy.png';
import medijarImg from './Medi-jar.png';
import MediphoneImg from './Medi-phone.png';
import MediclipImg from './Medi-clip.png';
import MedibagImg from './Medi-bag.png';
import MedireportImg from './Medi-report.png';
import MediserachImg from './Medi-serach.png';
import iconreloadImg from './sicon-reload.png';
import icongroupImg from './sicon-group.png';
import iconbellImg from './icon-bell.png';
import mapImg from './map.png';
import EllipseskyImg from './Vector-1.png';
import righttickImg from './right-tick.png';
import tickImg from './tick.png';
import EllipseoutlineImg from './Vector-outline.png';
import locationImg from './location.png';
import vector7Img from './Vector-7.png';
import vectorhalfImg from './Vector-half.png';
import vectorredImg from './Vector-red.png';

export {
    logoImg,
    mobileImg,
    deliveryImg,
    client1Img,
    client2Img,
    client3Img,
    client4Img,
    client5Img,
    client6Img,

}