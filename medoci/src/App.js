import './App.css';
import RouteNavigator from './router';

function App() {
  return (
            <RouteNavigator />
  );
}

export default App;
