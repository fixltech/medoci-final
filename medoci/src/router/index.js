import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import routes from "./routes";

function RouteWithSubRoutes(route) {
    return (
        <Route
            path={route.path}
            render={(props) => <route.component {...props} route={route.routes} />}
            />
    );
}

function RouteNavigator() {
    return (
      <Router>
        <Switch>
          {routes.map((route, routeIndex) => (
            <RouteWithSubRoutes key={routeIndex} {...route} />
          ))}
        </Switch>
      </Router>
    );
  }
  
  export default RouteNavigator;